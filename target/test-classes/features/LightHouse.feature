
  Feature: This is about running a performance audit of a web page
    @task1.2
    Scenario: Run a Lighthouse audit on the page https://www.vegasslotsonline.com/real-money/
      Given User navigate to the site with LightHouse extention
      When Select following settings
        | Categories | Performance |
        | Device     | Mobile      |
      Then Run the audit using Chrome’s Incognito mode.

