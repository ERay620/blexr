$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/features/SboClaim.feature");
formatter.feature({
  "name": "The bookie selector function",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "“CLAIM” button link is not broken with the following options",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@task2.3"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "User should be on sbo.net site.",
  "keyword": "Given "
});
formatter.match({
  "location": "SboClaimStepDefs.user_should_be_on_sbo_net_site()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on the Bookie Selector",
  "keyword": "When "
});
formatter.match({
  "location": "SboClaimStepDefs.user_click_on_the_Bookie_Selector()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Select the following following choices Desktop, €50 - €200, Yes",
  "keyword": "And "
});
formatter.match({
  "location": "SboClaimStepDefs.select_the_following_following_choices_Desktop_€_€_Yes(Integer,Integer)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Verify that “CLAIM” button link is not broken.",
  "keyword": "Then "
});
formatter.match({
  "location": "SboClaimStepDefs.verify_that_CLAIM_button_link_is_not_broken()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
});