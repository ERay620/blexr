@task2.2
Feature: Real Money Page Links

  Background:
    Given User should be see home page
    Then user should be on real money page

  Scenario:  Verify that all  of real money page  link correct page
    Then the following links on the site’s real money page have correct text
    |live casino|payment methods|selection of table games|New Online Slots|Our Providers|
    Then the following links on the site’s real money page link correct linkUrl
      | /live-dealer/ | /deposit-methods/ | /table-games/ | /real-money/ | /software-providers/ |




