Feature: The bookie selector function
  @task2.3
  Scenario: “CLAIM” button link is not broken with the following options

    Given User should be on sbo.net site.
    When  User click on the Bookie Selector
    And   Select the following following choices Desktop, €50 - €200, Yes
    Then  Verify that “CLAIM” button link is not broken.


