@task2.1
Feature: Verify the links in the site’s footer.

  Background:
    Given User should be see home page

  Scenario:  Verify that none of the links in the site’s footer is broken.
    Then the links in footer are shown below
      | Contact Us | Responsible Gambling | Fairness and Testing | Licensing Bodies and Regulators |Sitemap|Privacy Policy|

    And all links in footer are not broken