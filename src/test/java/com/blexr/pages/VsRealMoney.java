package com.blexr.pages;

import com.blexr.utilities.Driver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import java.util.ArrayList;
import java.util.List;

public class VsRealMoney {
    public VsRealMoney() {
    PageFactory.initElements(Driver.get(), this);
}

    @FindBy(xpath = ("//div[@class='grid-container']//a[contains(text(),'Real Money')]"))
    public WebElement realMoneyTab;

    @FindBy(xpath = "//a[contains(text(),'live casino')]")
    public WebElement liveCasino;

    @FindBy(xpath = "//a[contains(text(),'payment methods')]")
    public WebElement paymentMethods;

    @FindBy(xpath = "//a[contains(text(),'selection of table games')]")
    public WebElement tableGames;

    @FindBy(xpath = "//a[contains(text(),'New Online Slots')]")
    public WebElement newOnlineSlot;

    @FindBy(xpath = "//div[@class='right-side']//a[@class='cta-button transparent'][contains(text(),'Our Providers')]")
    public WebElement onlineProviders;


    public List<String> getLinkTexts(){
        List<String>linkTexts = new ArrayList<>();
        linkTexts.add(liveCasino.getText());
        linkTexts.add(paymentMethods.getText());
        linkTexts.add(tableGames.getText());
        linkTexts.add(newOnlineSlot.getText());
        linkTexts.add(onlineProviders.getText());
        return linkTexts;
    }

    public List<WebElement> clickGivenLinks(){
        List<WebElement>givenLinks = new ArrayList<>();
        givenLinks.add(liveCasino);
        givenLinks.add(paymentMethods);
        givenLinks.add(tableGames);
        givenLinks.add(newOnlineSlot);
        givenLinks.add(onlineProviders);
        return givenLinks;
    }

}
