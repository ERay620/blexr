package com.blexr.pages;

import com.blexr.utilities.Driver;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public class Lamda {
    public Lamda() {
        PageFactory.initElements(Driver.get(), this);
    }


    @FindBy(xpath ="//*[text()='Log in']" )
    public WebElement logIn;

    @FindBy(css = "[type=\"email\"]" )
    public  WebElement email;

    @FindBy(css = "[type=\"password\"]" )
    public  WebElement password;

    @FindBy(css = "[type=\"submit\"]" )
    public  WebElement submit;

    @FindBy(xpath ="//*[text()='Real Time Testing']" )
    public WebElement RealTimeTesting;

    @FindBy(css = "[class=\"fa fa-bars\"]" )
    public  WebElement urlInputBar;

    @FindBy(css = "[id=\"input-text\"]" )
    public  WebElement urlInput;

    @FindBy(css = "[src=\"assets/images/phone-2.svg\"]" )
    public  WebElement mobileDevice;

    @FindBy(css = "[class=\"fa fa-android\"]" )
    public  WebElement android;

    @FindBy(xpath ="//*[text()='Google']" )
    public WebElement google;

    @FindBy(xpath =" (//*[text()=' Nexus 6 '])[2]")
    public WebElement Nexus6;

    @FindBy(xpath =" (//*[@src=\"assets/images/internet-grey.svg\"])[2]")
    public WebElement internet;

    @FindBy(xpath ="(//*[@class=\"fa fa-apple\"])[2]")
    public WebElement apple;

    @FindBy(css = "[href=\"#ios-mobile\"]" )
    public  WebElement iphone;

    @FindBy(xpath ="(//*[text()=' iPhone 8 Plus '])[1]")
    public WebElement iPhone8Plus;

    @FindBy(xpath ="//*[text()=' 13.3 ']")
    public WebElement os13_3;

    @FindBy(xpath =" (//*[@src='assets/images/safari-grey.svg'])[3]")
    public WebElement safari;


    @FindBy(css = "[class=\"btn-start\"]" )
    public  WebElement startTest;

    JavascriptExecutor jse = (JavascriptExecutor) Driver.get();


    public void login() throws InterruptedException {
        Driver.get().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        logIn.click();
        email.sendKeys("erayberk80@gmail.com");
        password.sendKeys("12345678");
        submit.click();
    }

    public void androidCapability() throws InterruptedException {

        jse.executeScript("arguments[0].click();",android);
        jse.executeScript("arguments[0].click();",google);
        //jse.executeScript("arguments[0].click();",Nexus6);      // It has already been selected
        //jse.executeScript("arguments[0].click();",internet);   // It has already been selected
        jse.executeScript("arguments[0].click();",startTest);
        Thread.sleep(45000);

    }

    public void iphoneCapability() throws InterruptedException {

         jse.executeScript("arguments[0].click();",apple);
         jse.executeScript("arguments[0].click();",iphone);
        //jse.executeScript("arguments[0].click();",iPhone8Plus);    // It has already been selected
        //jse.executeScript("arguments[0].click();",safari);        // It has already been selected
        jse.executeScript("arguments[0].click();",startTest);
        Thread.sleep(45000);



    }

}