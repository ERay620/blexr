package com.blexr.pages;

import com.blexr.utilities.Driver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.Set;

public class SboClaim {
    public SboClaim() {
        PageFactory.initElements(Driver.get(), this);
    }

    @FindBy(css = ".kgw-header.for-picker>a>span" )
    public  WebElement BookieSelector;

    @FindBy(css = ".choice-button.choose-step:first-of-type" )
    public  WebElement desktop;

    @FindBy(xpath ="(//span[@class='choice-title'])[5]")
    public WebElement money50200;

    @FindBy(xpath ="(//span[@class='choice-title'])[8]")
    public WebElement yes;

    @FindBy(xpath ="//a[contains(text(),'Claim')]")
    public WebElement claim;

    @FindBy(xpath ="(//a[@class='back-link choose-step'])[3]")
    public WebElement startagain;

    public void switchWindow(){
        String currentWindowHandle=Driver.get().getWindowHandle();
        System.out.println("currentWindowHandle = " + currentWindowHandle);
        Set<String> windowHandles= Driver.get().getWindowHandles();
        System.out.println(windowHandles.size());
        for (String handle : windowHandles) {
            if(!currentWindowHandle.equals(handle)){
                Driver.get().switchTo().window(handle);
            }
        }

    }


}
