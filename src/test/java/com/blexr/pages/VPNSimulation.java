package com.blexr.pages;

import com.blexr.utilities.Driver;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.PageFactory;

import java.io.File;
import java.util.concurrent.TimeUnit;


public class VPNSimulation {

   public VPNSimulation() {
        PageFactory.initElements(Driver.get(), this);
    }


 public  void VPNSetting(String country) throws InterruptedException {
  WebDriverManager.chromedriver().setup();
  ChromeOptions options = new ChromeOptions();
  options.addExtensions(new File("src/test/resources/extension_3_9_1_0.crx"));

  DesiredCapabilities capabilities = new DesiredCapabilities();
  capabilities.setCapability(ChromeOptions.CAPABILITY, options);
  WebDriver driver = new ChromeDriver(capabilities);
  driver.manage().window().maximize();

  driver.get("chrome-extension://oofgbpoabipfcfjapgnbbjjaenockbdp/popup.html");
  driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

  JavascriptExecutor   jse = (JavascriptExecutor) driver;

  WebElement lang=driver.findElement(By.xpath("//*[text()='English']"));
  jse.executeScript("arguments[0].click();",lang);

  WebElement register=driver.findElement(By.xpath("//*[text()='Create an account']"));
  jse.executeScript("arguments[0].click();",register);

  WebElement privacyPolicy = driver.findElement(By.cssSelector("[id=\"pp-label\"]"));
  jse.executeScript("arguments[0].click();",privacyPolicy);

  WebElement terms =driver.findElement(By.cssSelector("[id=\"terms-label\"]"));
  jse.executeScript("arguments[0].click();",terms);

  WebElement licence=driver.findElement((By.cssSelector("[id=\"eula-label\"]")));
  jse.executeScript("arguments[0].click();",licence);

  WebElement register2=driver.findElement((By.cssSelector("[id=\"create-authcode\"]")));
  jse.executeScript("arguments[0].click();",register2);
  Thread.sleep(2000);
  WebElement login=driver.findElement(By.cssSelector("[id=\"login-with-authcode\"]"));
  jse.executeScript("arguments[0].click();",login);
  Thread.sleep(2000);

  System.out.println(country);

  if(country.equals("USA")){



   WebElement USA=driver.findElement(By.xpath("//*[text()='United States']"));
   jse.executeScript("arguments[0].click();",USA);
   driver.navigate().to("https://www.vegasslotsonline.com/real-money/");

   Thread.sleep(3000);
   driver.quit();

  }else if(country.equals("Italy")){
   WebElement Italy=driver.findElement(By.xpath("//*[text()='Italy']"));
   jse.executeScript("arguments[0].click();",Italy);
   driver.navigate().to("https://www.vegasslotsonline.com/real-money/");

   Thread.sleep(3000);
   driver.quit();

  }else if(country.equals("Germany")) {
   WebElement Germany=driver.findElement(By.xpath("//*[text()='Germany']"));
   jse.executeScript("arguments[0].click();",Germany);
   driver.navigate().to("https://www.vegasslotsonline.com/real-money/");

   Thread.sleep(3000);
   driver.quit();

  }

 }


}
