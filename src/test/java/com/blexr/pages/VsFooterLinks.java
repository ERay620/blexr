package com.blexr.pages;


import com.blexr.utilities.Driver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import java.util.ArrayList;
import java.util.List;

public class VsFooterLinks {
    public VsFooterLinks() {
        PageFactory.initElements(Driver.get(), this);
    }


    @FindBy(xpath = "//div[@class='col-md-9 bottom-menu']//ul//li")
    public List<WebElement> footerLinks;

    @FindBy(xpath = "//*[text()='Contact Us']")
    public WebElement ContactUs;

    @FindBy(xpath = "//*[text()='Privacy Policy']")
    public WebElement PrivacyPolicy;


    public List<String> footerLinksInfo(){
        List<String>footerLabelText= new ArrayList<>();
        for (WebElement label : footerLinks) {
            footerLabelText.add(label.getText());
        }
        return footerLabelText;
    }
}
