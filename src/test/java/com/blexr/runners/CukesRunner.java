package com.blexr.runners;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
 import org.junit.runner.RunWith;

@RunWith(Cucumber.class)  //it comes from JUnit
@CucumberOptions(

        plugin = {"json:target/cucumber.json",         //Where our feature files are located
                "html:target/defaut-html-reports",
                "rerun:target/rerun.txt"},
        features = "src/test/resources/features/",    //Gherking lang steps
        glue = "com/blexr/step_definitions/" ,       //Wrinting the methods to implement  the steps
        dryRun = false,
        tags = " @task2.3"
)

public class CukesRunner {


}
