package com.blexr.step_definitions;

import com.blexr.pages.VPNSimulation;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;


public class VPNSimulationStepDefs {
    VPNSimulation vpnSimulation = new VPNSimulation();

    @Then("User landed to the target page from Germany")
    public void user_landed_to_the_target_page_from_Germany() throws InterruptedException {
        vpnSimulation.VPNSetting("Germany");

    }

    @Then("User landed to the target page from Italy")
    public void user_landed_to_the_target_page_from_Italy() throws InterruptedException {
        vpnSimulation.VPNSetting("Italy");

    }

    @When("User landed to the target page from USA")
    public void user_landed_to_the_target_page_from_USA() throws InterruptedException {
        vpnSimulation.VPNSetting("USA");

    }

}


