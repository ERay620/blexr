package com.blexr.step_definitions;


import com.blexr.pages.Lamda;
import com.blexr.utilities.Driver;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class LambdaStepDefs {

     Lamda lamda = new Lamda();

    @Given("User should be login on lambdatest site.")
    public void user_should_be_login_on_lambdatest_site() throws InterruptedException {

         Driver.get().get("https://www.lambdatest.com/");
         lamda.login();

    }

    @When("User navigate to Real Time Testing")
    public void user_navigate_to_Real_Time_Testing()  {

        lamda.RealTimeTesting.click();

    }

    @Then("Enter you your site URL {string}")
    public void enter_you_your_site_URL(String urlOfVegas) throws InterruptedException {

          Thread.sleep(3000);
          lamda.urlInputBar.click();
          lamda.urlInput.sendKeys(urlOfVegas);
           Driver.get().manage().window().maximize();

    }

    @Then("Select Mobile Option")
    public void select_Mobile_Option() throws InterruptedException {

          Thread.sleep(3000);
          lamda.mobileDevice.click();

    }

    @When("Select following settings  Android, Google, {string}, Internet")
    public void select_following_settings_Android_Google_Internet(String string) throws InterruptedException {

        lamda.androidCapability();

    }


    @When("Select following settings  iOS, iPhone, {string}, Safari")
    public void select_following_settings_iOS_iPhone_Safari(String string) throws InterruptedException {
        lamda.iphoneCapability();
    }

    @Then("Aplication should be launch on the target device")
    public void aplication_should_be_launch_on_the_target_device() {

    }




}
