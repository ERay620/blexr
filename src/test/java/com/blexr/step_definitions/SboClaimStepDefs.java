package com.blexr.step_definitions;

import com.blexr.pages.SboClaim;
import com.blexr.utilities.Driver;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Arrays;

public class SboClaimStepDefs {
    SboClaim sboClaim=new SboClaim();


    @Given("User should be on sbo.net site.")
    public void user_should_be_on_sbo_net_site() {
        Driver.get().get("https://www.sbo.net/alternatives/");
        Driver.get().manage().window().maximize();

    }

    @When("User click on the Bookie Selector")
    public void user_click_on_the_Bookie_Selector() {
        sboClaim.BookieSelector.click();

    }

    @When("Select the following following choices Desktop, €{int} - €{int}, Yes")
    public void select_the_following_following_choices_Desktop_€_€_Yes(Integer int1, Integer int2) throws InterruptedException {

        sboClaim.desktop.click();
        sboClaim.money50200.click();
        sboClaim.yes.click();

        while(!sboClaim.claim.isDisplayed()){

            sboClaim.startagain.click();
            sboClaim.desktop.click();
            sboClaim.money50200.click();
            sboClaim.yes.click();
            Thread.sleep(2000);
        }





    }

    @Then("Verify that “CLAIM” button link is not broken.")
    public void verify_that_CLAIM_button_link_is_not_broken()  {

       sboClaim.claim.click();
        sboClaim.switchWindow();

        System.out.println("After switching : "+ Driver.get().getTitle());

        String expectedUrl="https://lp.22betpartners.com/p/sports-general/index_en.php?tag=d_88427m_8693c_SBO_Review";
        String actualUrl=Driver.get().getCurrentUrl();
        Assert.assertEquals(expectedUrl, actualUrl);
    }
}



