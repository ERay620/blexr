package com.blexr.step_definitions;

import com.blexr.pages.VsRealMoney;
import com.blexr.utilities.Driver;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebElement;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.assertEquals;


public class VsRealMoneyStepDefs {

    VsRealMoney realMoneyPage= new VsRealMoney();

    @Then("user should be on real money page")
    public void user_should_be_on_real_money_page() {

        realMoneyPage.realMoneyTab.click();
    String expedtedUrl="https://www.vegasslotsonline.com/real-money/";
    String actualUrl = Driver.get().getCurrentUrl();
    assertEquals(expedtedUrl, actualUrl);
}
    @Then("the following links on the site’s real money page have correct text")
    public void the_following_links_on_the_site_s_real_money_page_have_correct_text(List<String> linksText) {

        assertEquals(realMoneyPage.getLinkTexts(), linksText);
    }

    @Then("the following links on the site’s real money page link correct linkUrl")
    public void the_following_links_on_the_site_s_real_money_page_link_correct_linkUrl(List<String> linksPaths)  {

        List<String>actualPath= new ArrayList<>();
        for (WebElement links:realMoneyPage.clickGivenLinks()) {

            links.click();
            String actualURL = Driver.get().getCurrentUrl();
            String[] split = actualURL.split("https://www.vegasslotsonline.com");
            actualPath.add(split[1]);
            realMoneyPage.realMoneyTab.click();
        }
        assertEquals(linksPaths, actualPath);
    }

}
