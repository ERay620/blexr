package com.blexr.step_definitions;
import com.blexr.pages.VsFooterLinks;
import com.blexr.utilities.ConfigurationReader;
import com.blexr.utilities.Driver;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import java.util.List;
import static org.junit.Assert.assertEquals;


public class VsFooterLinksStepDefs {

    @Given("User should be see home page")
    public void user_should_be_see_home_page() {
        Driver.get().get(ConfigurationReader.get("home_page"));
        String expectedUrl = ConfigurationReader.get("home_page");
        String actualUrl = Driver.get().getCurrentUrl();
        assertEquals(expectedUrl, actualUrl);
    }
    @Then("the links in footer are shown below")
    public void the_links_in_footer_are_shown_below(List<String> footer) {
        VsFooterLinks vsFooterLinks = new VsFooterLinks();
        assertEquals(vsFooterLinks.footerLinksInfo(), footer);
    }

    @Then("all links in footer are not broken")
    public void all_links_in_footer_are_not_broken() throws InterruptedException {
        VsFooterLinks vsFooterLinks = new VsFooterLinks();
        Driver.get().manage().window().maximize();
        JavascriptExecutor jse= (JavascriptExecutor) Driver.get();

        jse.executeScript("window.scrollBy(0,500)");
        vsFooterLinks.ContactUs.click();
        Assert.assertEquals("https://www.vegasslotsonline.com/contact/",Driver.get().getCurrentUrl());

        jse.executeScript("window.scrollBy(0,500)");
        vsFooterLinks.PrivacyPolicy.click();
        Assert.assertEquals("https://www.vegasslotsonline.com/policy/",Driver.get().getCurrentUrl());


        for (int i = 1; i < vsFooterLinks.footerLinks.size(); i++) {
            jse.executeScript("window.scrollBy(0,500)");
            vsFooterLinks.footerLinks.get(i).click();
            Thread.sleep(2000);
            switch (i){

                case 1: Assert.assertEquals("https://www.vegasslotsonline.com/responsible-gambling/",Driver.get().getCurrentUrl());break;
                case 2:  Assert.assertEquals("https://www.vegasslotsonline.com/gaming-fairness-and-testing-companies/",Driver.get().getCurrentUrl());break;
                case 3:  Assert.assertEquals("https://www.vegasslotsonline.com/online-gaming-regulators-and-licensing-bodies/",Driver.get().getCurrentUrl());break;
                case 4:Assert.assertEquals("https://www.vegasslotsonline.com/sitemap.html",Driver.get().getCurrentUrl());break;

            }


        }
    }
}
