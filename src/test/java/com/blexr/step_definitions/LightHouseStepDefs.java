package com.blexr.step_definitions;

import com.blexr.pages.LightHouse;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import java.util.Map;


public class LightHouseStepDefs {

    public void LightHouseSetting() throws InterruptedException {
        LightHouse lightHouse = new LightHouse();
        lightHouse.LightHouseSetting();
    }

    @Given("User navigate to the site with LightHouse extention")
    public void user_navigate_to_the_site_with_LightHouse_extention() throws InterruptedException {

        LightHouseSetting();

    }

    @When("Select following settings")   //Missing Part
    public void select_following_settings(Map<String,String> auditSettings) { }

    @Then("Run the audit using Chrome’s Incognito mode.")  //I've made disabel for the sake of Lighthouse Extention
    public void run_the_audit_using_Chrome_s_Incognito_mode() { }
}


